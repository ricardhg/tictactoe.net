﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{

    public class TicTacToeOld : Game
    {
        private static string[] SIMBOLS = new string[3] { "-", "o", "x" };
        private static bool HUMAN = true;
        private static bool ROBOT = false;
        private static int PLAYER1 = 1;
        private static int PLAYER2 = 2;
        
        private static int[] map = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        private static int[][] WINNERS = {
            new int[3] {1,2,3},
            new int[3] {4,5,6},
            new int[3] {7,8,9},
            new int[3] {1,4,7},
            new int[3] {2,5,8},
            new int[3] {3,6,9},
            new int[3] {1,5,9},
            new int[3] {7,5,3}
        };

        /**
         * mira si a posicio n hi ha un zero
         */
        public static bool isZero(int n)
        {
            return (map[n - 1] == 0);
        }

        /**
         * estableix posició al mapa
         * @param posicio
         * @param valor
         */
        private void setMap(int posicio, int valor)
        {
            map[posicio - 1] = valor;
        }

        private int getMap(int posicio)
        {
            return map[posicio - 1];
        }

        private void tie()
        {
            Console.WriteLine("Nobody Wins...");
        }

        /**
         * retorna mapa en format text per mostrar a pantalla
         */
        public override string ToString()
        {
           
            StringBuilder sb = new StringBuilder();

            sb.Append(SIMBOLS[getMap(1)]).Append(" ").Append(SIMBOLS[getMap(2)]).Append(" ").Append(SIMBOLS[getMap(3)]).Append("\n");
            sb.Append(SIMBOLS[getMap(4)]).Append(" ").Append(SIMBOLS[getMap(5)]).Append(" ").Append(SIMBOLS[getMap(6)]).Append("\n");
            sb.Append(SIMBOLS[getMap(7)]).Append(" ").Append(SIMBOLS[getMap(8)]).Append(" ").Append(SIMBOLS[getMap(9)]).Append("\n");
            return sb.ToString();
        }

        private void draw()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine(this.ToString());
        }

        /**
         * retorna true si mapa ple
         * @return
         */
        public bool fullMap()
        {
            foreach (int i in map)
            {
                if (i == 0) return false;
            }
            return true;
        }

        /**
         * retorna 1/2 si algun jugador te jugada guanyadora
         * 0 si no 
         * @return
         */
        public int checkWinner()
        {
            int wins = 0;

            foreach (int[] winnerBet in WINNERS)
            {

                if (this.getMap(winnerBet[0]) == PLAYER1 && this.getMap(winnerBet[1]) == PLAYER1 && this.getMap(winnerBet[2]) == PLAYER1)
                { wins = 1; break; }

                if (this.getMap(winnerBet[0]) == PLAYER2 && this.getMap(winnerBet[1]) == PLAYER2 && this.getMap(winnerBet[2]) == PLAYER2)
                { wins = 2; break; }
            }
            return wins;
        }


        /**
         * retorna posició aleatoria triada entre les posicions zero
         * @return
         */
        private int randomPlay()
        {
            //creem List<int>
            List<int> al = getPositionsWithValue(0);

            //generem un random de 1 a mida List<int>
            Random random = new Random();
            int rnd = random.Next(0, al.Count) + 1;

            //retornem la posició triada (primer=0)
            return al[rnd - 1];
        }


        /**
         * retorna num de posicions amb valors que coincideixin amb els ints rebuts
         * @param values
         * @return
         */
        private List<int> getPositionsWithValue(params int[] values)
        {
            List<int> positions = new List<int>();
            for (int ii = 1; ii < 10; ii++)
            {
                foreach (int i in values)
                {
                    if (getMap(ii) == i) positions.Add(ii);
                }
            }
            return positions;
        }

        //el mateix, però retorna recompte
        private int countPositionsWithValue(params int[] values)
        {
            List<int> pos = getPositionsWithValue(values);

            return pos.Count;

        }

        /**
         * Cerca apostes guanyadores entre les posicions rebudes
         * positions és un List<int> de la forma [2,3,5,6,8,9]
         * on hi ha 0s i (1s o 2s) depen de jugador
         * i zerosLeft és el nombre de posicions "0" que acceptem per donar
         * la winnerbet com a vàlida
         * si acceptem només 1 seria jugada guanyadora
         * 
         * si no es troba jugada guanyadora es retorna 0
         * si es troba, es retornarà la posició que sempre anirà de 1 a 9
         * i que és la primera de les posicions 0 lliures (z.get(0))
         * 
         * possibles millores:
         *   no retornar z.get(0) sino la jugada estratègicament més interessant (en cas que hi hagi varios 0s)
         *   
         * @param positions
         * @param positionsLeft
         * @return
         */
        private int getWinnerBet(List<int> positions, int zerosLeft)
        {
            var w = new List<int>();
            var z = new List<int>();
            
            // per cada winnerbet mirem si està continguda en el array de 
            // posicions del jugador + posicions lliures
            //...
            foreach (int[] winnerBet in WINNERS)
            {
                //w es un array temporal q conté les posicions guanyadores
                for (int t = 0; t < 3; t++)
                {
                    w.Add(winnerBet[t]);
                }

                //w.ForEach(n => Console.WriteLine(n.ToString()));
                //java: if (positions.containsAll(w)){
                //if (!w.Intersect(positions).Any())
                if (positions.IndexOf(w[0])!=-1 
                    && positions.IndexOf(w[1])!=-1
                    && positions.IndexOf(w[2])!=-1 )
                {
                    z = getZeros(winnerBet);
                    if (z.Count == zerosLeft)
                    {
                        //System.out.println(w.toString());
                        return z[0];
                    }
                }
                w.Clear();
                z.Clear();
            }
            return 0;
        }


        /**
         * ia reforçada!
         * @return
         */
        private int iaPlay()
        {

            List<int> al;// = new List<int><Integer>();
            int resp;
            int iaPlayer = 2;
            int prioridad = 5;

            //en primer lloc mirem si es la nostra primera jugada
            if (countPositionsWithValue(iaPlayer) == 0)
            {
                if (getMap(prioridad) == 0)
                {
                    return prioridad; //si prioridad és lliure li donem prioritat
                }
                else
                {
                    return randomPlay(); // si és ocupat, random
                }
            }

            //mirem si NOSALTRES podem guanyar en 1!
            iaPlayer = 2;
            al = getPositionsWithValue(0, iaPlayer);
            resp = getWinnerBet(al, 1);
            if (resp > 0) return resp; //getwinnerbet retorna 0 si no troba posició guanyadora

            //mirem si el contrari pot guanyar en 1!
            //si és axí, li robem la jugada...
            iaPlayer = 1;
            al = getPositionsWithValue(0, iaPlayer);
            resp = getWinnerBet(al, 1);
            if (resp > 0) return resp;



            //si hem arribat fins aquí, vol dir que ningú pot guanyar en 1...
            iaPlayer = 2;
            al = getPositionsWithValue(0, iaPlayer);

            for (int pass = 2; pass < 4; pass++)
            {
                if (getWinnerBet(al, pass) > 0)
                {
                    return getWinnerBet(al, pass);
                }
            }

            Console.WriteLine("rnd");
            return randomPlay();
        }

        private List<int> getZeros(int[] arr)
        {
            List<int> zeros = new List<int>();
            foreach (int i in arr)
            {
                if (getMap(i) == 0) zeros.Add(i);
            }
            return zeros;
        }

        public void log (string str)
        {
            Console.WriteLine(str);
        }

        public void play()
        {

            TicTacToePlayer player1 = new TicTacToePlayer("Simple human", PLAYER1, HUMAN);
            TicTacToePlayer player2 = new TicTacToePlayer("IA Robot", PLAYER2, ROBOT);
            TicTacToePlayer currentPlayer;

            int position;

            currentPlayer = player1;

            do
            {
                // mirem si mapa és ple o tenim un guanyador, en aquest cas sortim
                if (fullMap() || checkWinner() != 0)
                {
                    draw();
                    break;
                }


                if (currentPlayer.isRobot())
                {
                    position = iaPlay();
                }
                else
                {
                    draw();
                    position = currentPlayer.play();
                }

                setMap(position, currentPlayer.getId());

                //conmuta currentPlayer entre player1 y player2
                currentPlayer = (currentPlayer == player1) ? player2 : player1;

                // mayorAB = (a>b) ? a : b;

                /*
                if (currentPlayer==player1) {
                    currentPlayer=player2;
                }else{
                    currentPlayer=player1;
                };
                */

            } while (true);

            

            switch (checkWinner())
            {
                case 1:
                    player1.wins();
                    break;
                case 2:
                    player2.wins();
                    break;
                default:
                    tie();
                    break;
            }

            Console.ReadKey();
        }
    }
}
