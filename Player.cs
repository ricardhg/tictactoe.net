﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public abstract class Player
    {

        private string name;
   
        protected string Name { get => name; set => name = value; }
     
        public Player(string name)
        {
            this.Name = name;
        }
        
        public abstract int play();
        public abstract void wins();

    }
}
