﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class TicTacToePlayer : Player
    {
        
    private int id;
    private bool human;

    public TicTacToePlayer(string name, int id, bool human) : base (name)
    {
        this.id = id;
        this.human = human;
    }

    public int getId()
    {
        return this.id;
    }

    public bool isHuman()
    {
        return this.human;
    }

    public bool isRobot()
    {
        return !this.human;
    }

   
    public override int play()
    {

        int jugada;
     
        do
        {
           Console.WriteLine(this.Name + " plays:");

           if (!int.TryParse(Console.ReadLine(), out jugada))
            { 
                jugada = 0;
            }

        } while (jugada==0 || !TicTacToe.isZero(jugada));

        return jugada;
    }



        
    public override void wins()
    {
            Console.WriteLine(this.Name + " wins!");

    }


}
}
